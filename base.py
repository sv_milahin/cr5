from typing import Any, Optional
from unit import BaseUnit


class BaseSingleton(type):
    _instances: dict[Any, Any] = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Arena(metaclass=BaseSingleton):
    STAMINA_PER_ROUND: str= 1
    player: BaseUnit= None
    enemy: BaseUnit = None
    game_is_running: bool = False

    def start_game(self, player: BaseUnit, enemy: BaseUnit) -> None:
        self.player = player
        self.enemy = enemy
        self.game_is_running = True

    def _check_players_hp(self):
        if self.player.hp > 0 and self.enemy.hp > 0:
            self.battle_result = "Ничья"
            return None
        if self.player.hp <= 0:
            self.battle_result = "Игрок проиграл битву"
            self.player.hp = 0
        elif self.enemy.hp <= 0:
            self.battle_result = "Игрок выиграл битву"
            self.enemy.hp = 0
        return self._end_game()

    def _stamina_regeneration(self) -> None:
        if self.player.unit_class.max_stamina > self.player.stamina:
            self.player.stamina += self.STAMINA_PER_ROUND * self.player.unit_class.stamina
            if self.player.unit_class.max_stamina < self.player.stamina:
                self.player.stamina = self.player.unit_class.max_stamina
        if self.enemy.unit_class.max_stamina > self.enemy.stamina:
            self.enemy.stamina += self.STAMINA_PER_ROUND * self.enemy.unit_class.stamina
            if self.enemy.unit_class.max_stamina < self.enemy.stamina:
                self.enemy.stamina = self.enemy.unit_class.max_stamina

    def next_turn(self):
        result = self._check_players_hp()
        if result is not None:
            return result

        self._stamina_regeneration()
        res = self.enemy.hit(self.player)
        result = self._check_players_hp()
        if result is not None:
            return result
        return res

    def _end_game(self)-> str:
        self._instances = {}
        self.game_is_running = False
        return self.battle_result

    def player_hit(self)-> str:
        result = self.player.hit(self.enemy)
        enemy_turn = self.next_turn()
        return f"{result}\n {enemy_turn}"

    def player_use_skill(self)-> str:
        result = self.player.use_skill(self.enemy)
        enemy_turn = self.next_turn()
        return f"{result}\n {enemy_turn}"

