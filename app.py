from flask import Flask, render_template, request, redirect, url_for

from classes import unit_classes
from equipment import Equipment
from unit import BaseUnit, PlayerUnit, EnemyUnit
from base import Arena


app = Flask(__name__)


heroes: dict[BaseUnit] = {}
arena = Arena() 


@app.route("/")
def menu_page():
    return render_template("index.html")


@app.route("/fight/")
def start_fight():
    arena.start_game(heroes["player"], heroes["enemy"])
    return render_template("fight.html", heroes=heroes)


@app.route("/fight/hit")
def hit():
    if arena.game_is_running:
        result = arena.player_hit()
    else:
        result = arena.battle_result
    return render_template("fight.html", heroes=heroes, result=result)


@app.route("/fight/use-skill")
def use_skill():
    if arena.game_is_running:
        result = arena.player_use_skill()
    else:
        result = arena.battle_result
    return render_template("fight.html", heroes=heroes, result=result)


@app.route("/fight/pass-turn")
def pass_turn():
    if arena.game_is_running:
        result = arena.next_turn()
    else:
        result = arena.battle_result
    return render_template("fight.html", heroes=heroes, result=result)


@app.route("/fight/end-fight")
def end_fight():
    return render_template("index.html", heroes=heroes)


@app.route("/choose-hero/", methods=["post", "get"])
def choose_hero():
    if request.method == 'GET':
        equipment = Equipment()
        result = {
            "header": "Выбери героя",
            "classes": unit_classes,
            "weapons": equipment.get_weapons_names(),  
            "armors": equipment.get_armors_names() 
        }
        return render_template("hero_choosing.html", result=result)
    if request.method == "POST":
        name = request.form["name"]
        classes = request.form["unit_class"]
        weapon = request.form["weapon"]
        armor = request.form["armor"]

        player = PlayerUnit(name=name, unit_class=unit_classes[classes])
        player.equip_weapon(weapon)
        player.equip_armor(armor)
        heroes["player"] = player
        return redirect(url_for("choose_enemy"))
    return ""



@app.route("/choose-enemy/", methods=["post", "get"])
def choose_enemy():
    if request.method == "GET":
        equipment = Equipment()
        result = {
            "header": "Выбери героя",
            "classes": unit_classes,
            "weapons": equipment.get_weapons_names(),  
            "armors": equipment.get_armors_names()  
        }
        return render_template("hero_choosing.html", result=result)
    if request.method == "POST":
        name = request.form["name"]
        classes = request.form["unit_class"]
        weapon = request.form["weapon"]
        armor = request.form["armor"]

        enemy = EnemyUnit(name=name, unit_class=unit_classes[classes])
        enemy.equip_weapon(weapon)
        enemy.equip_armor(armor)
        heroes["enemy"] = enemy
        return redirect(url_for("start_fight"))
    return ""


if __name__ == "__main__":
    app.run()
