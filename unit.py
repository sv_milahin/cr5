from __future__ import annotations
from abc import ABC, abstractmethod


from equipment import Equipment, Weapon, Armor
from classes import UnitClass
from random import randint
from typing import Optional


class BaseUnit(ABC):
    def __init__(self, name: str, unit_class: UnitClass):
        self.name: str = name
        self.unit_class: UnitClass = unit_class
        self.hp: float = unit_class.max_health
        self.stamina: float = unit_class.max_stamina
        self.weapon: Weapon | None = None
        self.armor: Armor | None = None
        self._is_skill_used: bool = False


    @property
    def skill_used(self) -> bool:
        return self._is_skill_used

    @property
    def health_points(self):
        return round(self.hp, 1)

    @property
    def stamina_points(self):
        return  round(self.stamina, 1)

    def equip_weapon(self, weapon: str)-> str:
        equipment = Equipment()
        self.weapon = equipment.get_weapon(weapon)
        return f"{self.name} экипирован оружием {self.weapon.name}"

    def equip_armor(self, armor: str)-> str:
        equipment = Equipment()
        self.armor = equipment.get_armor(armor)
        return f"{self.name} экипирован броней {self.armor.name}"

    def _count_damage(self, target: BaseUnit) -> Optional[float]:
        if not  self._checking_stamina_unit():
            return False
        if self._checking_stamina_target(target):
            self._stamina_changes_target(target)

            damage = round(self._get_unit_damage_with_weapons() - self._get_target_protection_with_armor(target), 1)
        else:
            damage = round(self._get_unit_damage_with_weapons(), 1)
        self._stamina_changes_unit()
        target.get_damage(damage)
        return damage

    def _checking_stamina_unit(self) -> bool:
        return self.stamina > self.weapon.stamina_per_hit



    def _get_unit_damage_with_weapons(self) -> float:
        return self.weapon.damage * self.unit_class.attack

    def _stamina_changes_unit(self) -> None:
        self.stamina -= self.weapon.stamina_per_hit
        if self.stamina < 0:
            self.stamina =0

    def _checking_stamina_target(self, target: BaseUnit) -> bool:
        return target.stamina > target.armor.stamina_per_turn

    def _get_target_protection_with_armor(self, target: BaseUnit) -> float:
        return target.armor.defence * target.unit_class.armor

    def _stamina_changes_target(self, target: BaseUnit) -> None:
        target.stamina -= target.armor.stamina_per_turn

    def get_damage(self, damage: int) -> Optional[float]:
        if not damage:
            return False
        if damage > 0:
            self.hp -= damage
            return damage
        return damage


    @abstractmethod
    def hit(self, target: BaseUnit) -> str:
        pass

    def use_skill(self, target: BaseUnit) -> str:
        if self.skill_used:
            return "Навык использован"

        self._is_skill_used = True
        return self.unit_class.skill.use(user=self, target=target)


class PlayerUnit(BaseUnit):
    def hit(self, target: BaseUnit) -> str:
        damage = self._count_damage(target)

        if not damage:
            return f"{self.name} попытался использовать {self.weapon.name}, но у него не хватило выносливости."

        if damage > 0:
            return f"{self.name} используя {self.weapon.name} пробивает {target.armor.name}" \
                   f" соперника и наносит {damage} урона."

        return f"{self.name} используя {self.weapon.name} наносит удар, но {target.armor.name} " \
               f"cоперника его останавливает."


class EnemyUnit(BaseUnit):

    def hit(self, target: BaseUnit) -> str:
        if not self._is_skill_used and randint(0, 1):
                return self.use_skill(target=target)

        damage = self._count_damage(target)
        if not damage:
            return f"{self.name} попытался использовать {self.weapon.name}, но у него не хватило выносливости."

        if damage > 0:
            return f"{self.name} используя {self.weapon.name} пробивает {target.armor.name} и наносит Вам {damage} урона."

        return f"{self.name} используя {self.weapon.name} наносит удар, но Ваш(а) {target.armor.name} его останавливает."


